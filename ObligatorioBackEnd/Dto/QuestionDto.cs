using ObligatorioBackEnd.Models;

namespace ObligatorioBackEnd.Dto
{
    public class QuestionDto
    {
        public int QuestionId{get;set;}
        public string Question{get;set;}
        public string Answer{get;set;}
        public QuestionDto (){}
        public QuestionDto (Question question){
            QuestionId=question.Id;
            Question=question.Text;
            Answer=question.Answer;
        }
    }
}