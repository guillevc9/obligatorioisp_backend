using System.Collections.Generic;
using ObligatorioBackEnd.Models;

namespace ObligatorioBackEnd.DataContext
{
    public interface IRepository
    {
        List<Publication> GetAllPublications();
        List<Publication> GetFilterPublications(string category, string zone);
        Publication GetPublicationById(int id);
        List<Publication> GetPublicationsCreatedByUser(string googleId);
        int SaveChanges();
        int AddPublication(Publication pub);
        Category GetCategoryByName(string category);
        List<Category> GetAllCategories();
        Zone GetZoneByName(string zone);
        List<Zone> GetAllZones();
        User GetUserByGoogleId(string googleId);
        int AddUser(User user);
        List<Question> GetQuestionsByPublication(int publicationId);
        Question GetQuestionByPublication(int publicationId, int questionId);
        bool isUserAnnotatedInPublication(int publicationId, string googleId);
        List<Publication> GetPublicationsAnnotatedByUser(string googleId);
        List<User> GetAnnotatedUsersByPublication(int publicationId);
        int FinalizePublication(int publicationId, FinalizePublication finalizePublication);
        string GetFcmTokenByUser(string googleId);
    }
}