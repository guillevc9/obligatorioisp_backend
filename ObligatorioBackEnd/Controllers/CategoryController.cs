using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ObligatorioBackEnd.DataContext;
using ObligatorioBackEnd.Dto;

namespace ObligatorioBackEnd.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController:ControllerBase
    {
        private  IRepository _repository;
        public CategoryController(IRepository repository){
            _repository=repository;
        }

        [HttpGet]
        public IActionResult GetAllCategories(){
            var listCategoriesDto = new List<CategoryDto>();
            foreach(var cat in _repository.GetAllCategories()){
                listCategoriesDto.Add(new CategoryDto(cat));
            }
            return Ok(listCategoriesDto);
        }
    }
}