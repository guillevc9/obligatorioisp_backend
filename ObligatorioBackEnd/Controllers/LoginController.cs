using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Google.Apis.Auth;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using ObligatorioBackEnd.DataContext;
using ObligatorioBackEnd.Models;


namespace ObligatorioBackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private IRepository _repository;
        public LoginController(IRepository repository){
            _repository=repository;
        }

        [HttpGet]
        public async Task<IActionResult> Login()
        {    
            Boolean validGoogleToken = false;
            String authToken = Request.Headers[HeaderNames.Authorization];

            //Verifico si esta en token obtenidos
            var tokenDictionary = TokenMap.Instance;

            if (tokenDictionary.Exists(authToken))
            {
                //Si existe continuo con la peticion original
                Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: El token existe en tokenmap");
                return Ok("El token existe en tokenmap");
            }
            else if(authToken!="")
            {
                try
                {
                    await GoogleJsonWebSignature.ValidateAsync(authToken);
                    validGoogleToken = true;
                }
                catch (InvalidJwtException e)
                {
                    String error = String.Format("{0} - {1}", e.Message, e.StackTrace);
                    return Unauthorized(error);
                }

                //Si no existe cheque el token contra Google
                if (validGoogleToken)
                {
                    //Si Google valida el token, entonces ahora lo valido contra mi api key
                    var jwtHandler = new JwtSecurityTokenHandler();
                    var readableToken = jwtHandler.CanReadToken(authToken);
                    if (readableToken != true)
                    {
                        Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: El token no contiene un formato JWT adecuado");
                        return Unauthorized("El token no contiene un formato JWT adecuado");
                    }
                    else
                    {
                        var jwt_token = jwtHandler.ReadJwtToken(authToken);
                        //Extract the headers of the JWT
                        var claims = jwt_token.Claims.ToList();
                        //Claim aud = claims
                        var claimAud = claims.Where(c => c.Type == "aud").Single();
                        //Chequeo aud contra mi api key
                        if (claimAud.Value == Data.GoogleAuthKey)
                        {
                            var claimGoogleId = claims.Where(c => c.Type == "sub").First<Claim>();                                                        

                            //if(!_dbContext.Usuarios.Any(u => u.Id_Google == claimGoogleId.Value))
                            if(_repository.GetUserByGoogleId(claimGoogleId.Value)==null)
                            {
                                Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: Entro para agregar el usuario");
                                //Si no existe el usuario lo agrego a la BD
                                var user = new User();
                                user.Name= claims.Where(c => c.Type == "given_name").Single().Value;
                                user.LastName=claims.Where(c => c.Type == "family_name").Single().Value;
                                user.Mail=claims.Where(c => c.Type == "email").Single().Value;
                                user.Id_Google  =claimGoogleId.Value;
                                _repository.AddUser(user);
                            }else{
                                Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: NO entro para agregar el usuario");
                            }

                            //El metodo Token_agregar se fija si existe el id reemplaza el token, si no 
                            //lo agrega como nuevo
                            TokenMap.Instance.Add(claimGoogleId.Value, authToken);
                            Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: El token es correcto");
                            return Ok("El token es correcto");
                        }
                        else
                        {
                            Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: El token no proviene de la aplicacion");
                            return Unauthorized("El token no proviene de la aplicación" );
                        }
                    }
                }
                else
                {
                    Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: El token no puede ser validado por Google");
                    return Unauthorized("El token no puede ser validado por Google" );
                }
            }else{
                Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: El token no puede ser vacio");
                return Unauthorized("El token no puede ser vacio" );
            }
        }
    }
}