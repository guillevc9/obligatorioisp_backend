using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ObligatorioBackEnd.Controllers;
using ObligatorioBackEnd.DataContext;
using ObligatorioBackEnd.Dto;
using ObligatorioBackEnd.Models;
using Xunit;

namespace ObligatorioBackend.Tests
{
    public class CategoryControllerTest
    {
        [Fact]
        public void GetAllCategories_ShouldListNoCategories()
        {
            var options = new DbContextOptionsBuilder<EFDataContext>()
                .UseInMemoryDatabase(databaseName: "CategoryController_NoCategories")
                .Options;

            IRepository repo = new Repository(new EFDataContext(options));

            var controller = new CategoryController(repo);

            var result = Assert.IsType<OkObjectResult>(controller.GetAllCategories());

            //var model = Assert.IsType<List<CategoryDto>>(result.Value);
            var listCategories = result.Value as List<CategoryDto>;

            Assert.Empty(listCategories);

        }

        [Fact]
        public void GetAllCategories_ShouldListAllCategories()
        {
            var options = new DbContextOptionsBuilder<EFDataContext>()
                           .UseInMemoryDatabase(databaseName: "CategoryController")
                           .Options;

            var inMemoryDb = new EFDataContext(options);

            Category cat1 = new Category { Name = "Categoria1" };
            Category cat2 = new Category { Name = "Categoria2" };
            Category cat3 = new Category { Name = "Categoria3" };
            inMemoryDb.Categories.Add(cat1);
            inMemoryDb.Categories.Add(cat2);
            inMemoryDb.Categories.Add(cat3);
            inMemoryDb.SaveChanges();

            IRepository repo = new Repository(inMemoryDb);

            var controller = new CategoryController(repo);

            var result = Assert.IsType<OkObjectResult>(controller.GetAllCategories());

            //var model = Assert.IsType<List<CategoryDto>>(result.Value);
            var listCategories = result.Value as List<CategoryDto>;
            Assert.Equal<int>(3, listCategories.Count);

            //inMemoryDb.Database.EnsureDeleted
        }

    }
}