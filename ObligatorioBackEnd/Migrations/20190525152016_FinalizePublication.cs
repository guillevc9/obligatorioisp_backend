﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ObligatorioBackEnd.Migrations
{
    public partial class FinalizePublication : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "FinalizePublication",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Reason = table.Column<string>(nullable: true),
                    UserAssignedUsuarioId = table.Column<int>(nullable: true),
                    OtherReason = table.Column<string>(nullable: true),
                    IdPublicacion = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FinalizePublication", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FinalizePublication_Publicaciones_IdPublicacion",
                        column: x => x.IdPublicacion,
                        principalTable: "Publicaciones",
                        principalColumn: "PublicacionId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FinalizePublication_Usuarios_UserAssignedUsuarioId",
                        column: x => x.UserAssignedUsuarioId,
                        principalTable: "Usuarios",
                        principalColumn: "UsuarioId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FinalizePublication_IdPublicacion",
                table: "FinalizePublication",
                column: "IdPublicacion",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FinalizePublication_UserAssignedUsuarioId",
                table: "FinalizePublication",
                column: "UserAssignedUsuarioId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FinalizePublication");
        }
    }
}
