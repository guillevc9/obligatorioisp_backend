namespace ObligatorioBackEnd.Models
{
    public class AnnotatedUsers
    {
        public int UserId {get;set;}
        public int PublicationId{get;set;}
        public Publication Publication {get;set;}
        public User User{get;set;}

        public AnnotatedUsers(){}

        public AnnotatedUsers(Publication pub, User usu){
            Publication=pub;
            User=usu;
        }
    }
}