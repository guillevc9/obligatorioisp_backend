using System;
using System.Linq;
namespace ObligatorioBackEnd
{
    class CleanString
    {
        public static string Clean(string dirtyString)
        {
                return new String(dirtyString.Where(Char.IsLetterOrDigit).ToArray());
        }
    }
}