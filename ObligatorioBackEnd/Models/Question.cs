using System.ComponentModel.DataAnnotations;
using ObligatorioBackEnd.Dto;

namespace ObligatorioBackEnd.Models
{
    public class Question
    {
        public int Id {get;set;}
        [Required]
        [StringLength(140)]
        public string Text {get;set;}
        [StringLength(140)]
        public string Answer {get;set;}
        public Publication Publication {get;set;}
        public int IdPublication {get;set;}

        public Question(){}
        public Question(QuestionDto questionDto){
            Text=questionDto.Question;
            Answer=questionDto.Answer;
        }
    }
}