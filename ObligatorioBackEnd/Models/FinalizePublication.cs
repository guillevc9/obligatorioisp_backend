namespace ObligatorioBackEnd.Models
{
    public class FinalizePublication
    {
        public int Id {get;set;}
        public string Reason {get;set;}
        public User UserAssigned {get;set;}
        public string OtherReason {get;set;}
        public Publication Publication {get;set;}
        public int IdPublication {get;set;}

    }
}