using System;
using ObligatorioBackEnd.Models;

namespace ObligatorioBackEnd.Dto
{
    public class PublicationDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public string Category { get; set; }
        public string Zone { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string UserFamilyName { get; set; }
        public Boolean Active { get; set; }
        public string Reason { get; set; }
        public string UserAssignedId {get;set;}
        public string UserAssigned { get; set; }
        public string OtherReason { get; set; }
        public int Photos {get;set;}


        public PublicationDTO() { }

        public PublicationDTO(Publication pub)
        {
            Id = pub.PublicationId;
            Title = pub.Title;
            Description = pub.Description;
            Address = pub.Address;
            Category = pub.Category.Name;
            Zone = pub.Zone.Name;
            UserId = pub.User.Id_Google;
            UserName = pub.User.Name;
            UserFamilyName = pub.User.LastName;
            Active = pub.Active;
            Photos=pub.Photos;

            if (Active)
            {
                Reason = "";
                UserAssigned = "";
                UserAssignedId="";
                OtherReason = "";
            }
            else
            {
                Reason = pub.FinalizeReason.Reason;
                if (Reason == "Seleccionar candidato")
                {
                    UserAssigned = String.Format("{0} {1}", pub.FinalizeReason.UserAssigned.Name, pub.FinalizeReason.UserAssigned.LastName);
                    UserAssignedId = pub.FinalizeReason.UserAssigned.Id_Google;
                }
                else
                {
                    UserAssigned = "";
                    UserAssignedId="";
                }
                OtherReason = pub.FinalizeReason.OtherReason;
            }
        }
    }
}