using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using FCM.Net;
using ObligatorioBackEnd.DataContext;

namespace ObligatorioBackEnd
{
    public class PushNotification
    {
        IRepository _repository;

        public PushNotification(IRepository repository){
            _repository=repository;
        }
        public async Task SendNewQuestionNotification(string googleId)
        {
            var userFcmToken = _repository.GetFcmTokenByUser(googleId);
            var sender=new Sender(Data.FireBaseKey);

            var message=new Message{
                RegistrationIds= new List<string> { userFcmToken},
                Notification = new Notification{
                    Title="Nueva pregunta",
                    Body ="Se ha ingresado una nueva pregunta en una de sus publicaciones" 
                }
            };
            var result= await sender.SendAsync(message);
            //result.MessageResponse.Success
        }

        public async Task SendSelectedCandidateNotification(string googleId)
        {
            var userFcmToken = _repository.GetFcmTokenByUser(googleId);
            var sender=new Sender(Data.FireBaseKey);

            var message=new Message{
                RegistrationIds= new List<string> { userFcmToken},
                Notification = new Notification{
                    Title="Ha sido seleccionado",
                    Body ="Ud. ha sido seleccionado en una de las publicaciones en las cuales se anoto" 
                }
            };
            var result= await sender.SendAsync(message);
        }
    }
}