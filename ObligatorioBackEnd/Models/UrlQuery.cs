namespace ObligatorioBackEnd.Models
{
    public class UrlQuery
{
    public string Category { get; set; }
    public string Zone { get; set; }
    public UrlQuery (){}

    public UrlQuery(string category, string zone){
        Category=category;
        Zone=zone;
    }
 
    public bool HaveFilter => !string.IsNullOrEmpty(Category) || !string.IsNullOrEmpty(Zone);
}
}