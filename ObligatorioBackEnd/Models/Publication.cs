﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using ObligatorioBackEnd.DataContext;
using ObligatorioBackEnd.Dto;
using ObligatorioBackEnd.Properties;

namespace ObligatorioBackEnd.Models 
{
    public class Publication
    {
        [Key]
        public int PublicationId { get; set; }

        [Required(ErrorMessageResourceName = "Pub_Titulo_Req", ErrorMessageResourceType = typeof(Errores))]
        [StringLength(40, ErrorMessageResourceName = "Pub_Titulo_LargoMax", ErrorMessageResourceType = typeof(Errores))]
        public string Title { get; set; }

        [StringLength(200, ErrorMessageResourceName = "Pub_Descripcion_LargoMax", ErrorMessageResourceType = typeof(Errores))]
        public string Description { get; set; }

        [Required(ErrorMessageResourceName = "Pub_Direccion_Req", ErrorMessageResourceType = typeof(Errores))]
        public string Address { get; set; }

        [Required]
        public Category Category {get;set;}
        [Required]
        public User User {get;set;}
        public Zone Zone{get;set;}
        public ICollection<AnnotatedUsers> AnnotatedUsers {get;set;}
        public Boolean Active {get;set;}
        public FinalizePublication FinalizeReason {get;set;}
        public int Photos {get;set;}
        public ICollection<Question> Questions {get;set;}
      
        public Publication(PublicationDTO pubDto){
            PublicationId=0;
            Title=pubDto.Title;
            Description=pubDto.Description;
            Address=pubDto.Address;
            Active=pubDto.Active;
            Photos=pubDto.Photos;
        }
        public Publication(){
            
        }

       
    }
}



