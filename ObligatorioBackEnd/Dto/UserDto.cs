using ObligatorioBackEnd.Models;

namespace ObligatorioBackEnd.Dto
{
    public class UserDto
    {
        public string Name {get;set;}
        public string FamilyName {get;set;}
        public string IdGoogle {get;set;}

        public UserDto(){}

        public UserDto(User user){
            Name=user.Name;
            FamilyName=user.LastName;
            IdGoogle=user.Id_Google;
        }
    }
}