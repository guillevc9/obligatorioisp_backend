﻿using Microsoft.EntityFrameworkCore;
using ObligatorioBackEnd.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ObligatorioBackEnd.DataContext
{
    public class EFDataContext : DbContext
    {

        public EFDataContext()
        {

        }
        public EFDataContext(DbContextOptions<EFDataContext> options) : base(options)
        { 
            
        }

        private const string ConnectionStringLinuxTmp = @"data source=localhost; initial catalog=obligatorio;persist security info=True;user id=sa;password=Obligatorio1";
        private const string ConnectionStringWin = @"Server=DESKTOP-PD700B6\SQLEXPRESS;Database=obligatorio;User Id=obligatorio;Password=obligatorio2019";
        private const string ConnectionStringWinAntel = @"Server=U0B06071\SQLEXPRESS;Database=obligatorio;User Id=obligatorio;Password=obligatorio2019";

        public DbSet<Publication> Publications { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Zone> Zones { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AnnotatedUsers>()
                .HasKey(au => new { au.PublicationId, au.UserId });
            modelBuilder.Entity<AnnotatedUsers>()
                .HasOne(au => au.Publication)
                .WithMany(p => p.AnnotatedUsers)
                .HasForeignKey(au => au.PublicationId);
            modelBuilder.Entity<AnnotatedUsers>()
                .HasOne(au => au.User)
                .WithMany(u => u.AnnotatedPublications)
                .HasForeignKey(au => au.UserId);
            modelBuilder.Entity<Publication>()
                .HasOne(p => p.FinalizeReason)
                .WithOne(fp => fp.Publication)
                .HasForeignKey<FinalizePublication>(fp => fp.IdPublication);
            modelBuilder.Entity<Publication>()
                .HasMany(p => p.Questions)
                .WithOne(q => q.Publication)
                .HasForeignKey(q => q.IdPublication);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(connectionString: ConnectionStringLinuxTmp);
            }
        }

    }
}
