namespace ObligatorioBackEnd.Dto
{
    public class FinalizePublicationDto
    {
        public int IdPublication {get;set;}
        public string Reason {get;set;}
        public string IdGoogleUser {get;set;}
        public string OtherReason {get;set;}

        public FinalizePublicationDto(){}
    
    }
}