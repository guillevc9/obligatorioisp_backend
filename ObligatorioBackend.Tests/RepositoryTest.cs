using Xunit;
using ObligatorioBackEnd.DataContext;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using ObligatorioBackEnd.Models;
using Xunit.Abstractions;
using System.Linq;

namespace ObligatorioBackend.Tests
{
    public class RepositoryTest
    {
        private readonly ITestOutputHelper output;
        public RepositoryTest(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void GetAllPublications_ShouldListWithZeroPublications()
        {
            var options = new DbContextOptionsBuilder<EFDataContext>()
                .UseInMemoryDatabase(databaseName: "RepositoryTest_NoPublications")
                .Options;

            IRepository repo = new Repository(new EFDataContext(options));

            List<Publication> pubsDtos = repo.GetAllPublications();
            Assert.Empty(pubsDtos);
        }

        [Fact]
        public void GetAllPublications_ShouldListWithOnePublication()
        {
            var options = new DbContextOptionsBuilder<EFDataContext>()
               .UseInMemoryDatabase(databaseName: "RepositoryTest_Publications")
               .Options;

            var inMemory = new EFDataContext(options);

            IRepository repo = new Repository(inMemory);

            User usr = new User
            {
                Name = "Usuario",
                LastName = "De Prueba",
                Mail = "usuario@deprueba.com",
                Id_Google = "1234",
                Fcm_token = "1234",
                AnnotatedPublications = null
            };

            Category cat = new Category
            {
                Name = "Categoria1"
            };

            Zone zon = new Zone
            {
                Name = "Zone1"
            };

            Publication pub = new Publication
            {
                Active = true,
                AnnotatedUsers = null,
                Category = cat,
                Zone = zon,
                Description = "Una descripcion",
                Address = "Una direccion",
                FinalizeReason = null,
                Photos = 0,
                Questions = new List<Question>(),
                Title = "Un titulo",
                User = usr
            };


            inMemory.Publications.Add(pub);

            int cant = inMemory.SaveChanges();

            List<Publication> pubsDtos = repo.GetAllPublications();
            Assert.Single(inMemory.Publications);
        }
        [Fact]
        public void GetCategoryByName_ShouldReturnOneCategory()
        {
            var options = new DbContextOptionsBuilder<EFDataContext>()
                .UseInMemoryDatabase(databaseName: "RepositoryTest_Categories")
                .Options;
            var inMemory = new EFDataContext(options);

            Category cat = new Category
            {
                Name = "Categoria1"
            };

            inMemory.Categories.Add(cat);
            inMemory.SaveChanges();

            IRepository repo = new Repository(inMemory);
            Category cate = repo.GetCategoryByName("Categoria1");
            Assert.NotNull(cate);

        }
        [Fact]
        public void GetCategoryByName_ShouldReturnNoCategory()
        {
            var options = new DbContextOptionsBuilder<EFDataContext>()
                .UseInMemoryDatabase(databaseName: "RepositoryTest_Categories")
                .Options;
            var inMemory = new EFDataContext(options);

            IRepository repo = new Repository(inMemory);
            Category cate = repo.GetCategoryByName("Categoria3");
            Assert.Null(cate);
        }

        [Fact]
        public void GetZoneByName_ShouldReturnOneZone()
        {
            var options = new DbContextOptionsBuilder<EFDataContext>()
                .UseInMemoryDatabase(databaseName: "RepositoryTest_Zones")
                .Options;
            var inMemory = new EFDataContext(options);

            var zone = new Zone
            {
                Name = "Zone1"
            };

            inMemory.Zones.Add(zone);
            inMemory.SaveChanges();

            IRepository repo = new Repository(inMemory);
            Zone zon = repo.GetZoneByName("Zone1");
            Assert.NotNull(zon);

        }
        [Fact]
        public void GetZoneByName_ShouldReturnNoZone()
        {
            var options = new DbContextOptionsBuilder<EFDataContext>()
                .UseInMemoryDatabase(databaseName: "RepositoryTest_Zones")
                .Options;
            var inMemory = new EFDataContext(options);

            IRepository repo = new Repository(inMemory);
            Zone zone = repo.GetZoneByName("Zone3");
            Assert.Null(zone);
        }


        [Fact]
        public void AddPublication_ShouldAddPublication()
        {
            var options = new DbContextOptionsBuilder<EFDataContext>()
                .UseInMemoryDatabase(databaseName: "addPublication")
                .Options;
            var inMemory = new EFDataContext(options);

            User usr = new User
            {
                Name = "Usuario",
                LastName = "De Prueba",
                Mail = "usuario@deprueba.com",
                Id_Google = "1234",
                Fcm_token = "1234",
                AnnotatedPublications = null
            };

            Category cat = new Category
            {
                Name = "Categoria1"
            };

            Zone zon = new Zone
            {
                Name = "Zone1"
            };

            Publication pub = new Publication
            {
                Active = true,
                AnnotatedUsers = null,
                Category = cat,
                Zone = zon,
                Description = "Una descripcion",
                Address = "Una direccion",
                FinalizeReason = null,
                Photos = 0,
                Questions = new List<Question>(),
                Title = "Un titulo",
                User = usr
            };

            IRepository repo = new Repository(inMemory);

            repo.AddPublication(pub);
            Assert.Single(inMemory.Publications);
        }

        [Fact]
        public void GetPublicationById_ShouldReturnOnePublication()
        {
            var options = new DbContextOptionsBuilder<EFDataContext>()
                .UseInMemoryDatabase(databaseName: "getPublicationById")
                .Options;
            var inMemory = new EFDataContext(options);

            var usu = new User
            {
                Name = "Usuario",
                LastName = "De Prueba",
                Mail = "usuario@deprueba.com",
                Id_Google = "1234",
                Fcm_token = "1234",
                AnnotatedPublications = null
            };

            var cat1 = new Category
            {
                Name = "Categoria1"
            };

            var cat2 = new Category
            {
                Name = "Categoria2"
            };


            var zone1 = new Zone
            {
                Name = "Zone1"
            };

            var zone2 = new Zone
            {
                Name = "Zone2"
            };

            Publication pub1 = new Publication
            {
                Active = true,
                AnnotatedUsers = null,
                Category = cat1,
                Zone = zone1,
                Description = "Una descripcion",
                Address = "Una direccion",
                FinalizeReason = null,
                Photos = 0,
                Questions = new List<Question>(),
                Title = "Un titulo1",
                User = usu
            };

            Publication pub2 = new Publication
            {
                Active = true,
                AnnotatedUsers = null,
                Category = cat2,
                Zone = zone2,
                Description = "Una descripcion",
                Address = "Una direccion",
                FinalizeReason = null,
                Photos = 0,
                Questions = new List<Question>(),
                Title = "Un titulo2",
                User = usu
            };

            Publication pub3 = new Publication
            {
                Active = true,
                AnnotatedUsers = null,
                Category = cat1,
                Zone = zone2,
                Description = "Una descripcion",
                Address = "Una direccion",
                FinalizeReason = null,
                Photos = 0,
                Questions = new List<Question>(),
                Title = "Un titulo3",
                User = usu
            };

            inMemory.Users.Add(usu);
            inMemory.Categories.Add(cat1);
            inMemory.Categories.Add(cat2);
            inMemory.Zones.Add(zone1);
            inMemory.Zones.Add(zone2);
            inMemory.Publications.Add(pub1);
            inMemory.Publications.Add(pub2);
            inMemory.Publications.Add(pub3);
            inMemory.SaveChanges();
            IRepository repo = new Repository(inMemory);

            output.WriteLine($"Recorriendo publicaciones");
            var pubs= inMemory.Publications.ToList();
            foreach (var pub in pubs)
            {
                output.WriteLine($"Titulo: {pub.Title} {pub.PublicationId}");
            }

            var pubObtain=repo.GetPublicationById(3);
            var pubRes =inMemory.Publications.Where(p=>p.PublicationId==3).SingleOrDefault();

            output.WriteLine($"Publicacion1: {pubRes.Title}");
            output.WriteLine($"Publicacion2: {pubObtain.Title}");
            Assert.Equal(pubObtain, pubRes);

        }

        [Fact]
        public void GetPublicationById_ShouldReturnNoPublication()
        {

          var options = new DbContextOptionsBuilder<EFDataContext>()
                .UseInMemoryDatabase(databaseName: "getPublicationById")
                .Options;
            var inMemory = new EFDataContext(options);

        IRepository repo =new Repository(inMemory);
        var pub = repo.GetPublicationById(1000);

        Assert.Null(pub);
        }

        [Fact]
        public void GetPublicationsByUser_shouldReturnThreePublications()
        {

          var options = new DbContextOptionsBuilder<EFDataContext>()
                .UseInMemoryDatabase(databaseName: "getPublicationById")
                .Options;
            var inMemory = new EFDataContext(options);

        IRepository repo =new Repository(inMemory);
        var pubs = repo.GetPublicationsCreatedByUser("1234");

        int quantity= pubs.Count;
        Assert.Equal(3,quantity);
        }

        [Fact]
        public void GetPublicationsByUser_shouldReturnNoPublications()
        {
          var options = new DbContextOptionsBuilder<EFDataContext>()
                .UseInMemoryDatabase(databaseName: "getPublicationById")
                .Options;
            var inMemory = new EFDataContext(options);

        IRepository repo =new Repository(inMemory);
        var pubs = repo.GetPublicationsCreatedByUser("4321");

        Assert.Empty(pubs);
        }

         [Fact]
        public void GetUserByGoogleId_shouldReturnOneUser()
        {

          var options = new DbContextOptionsBuilder<EFDataContext>()
                .UseInMemoryDatabase(databaseName: "getPublicationById")
                .Options;
            var inMemory = new EFDataContext(options);

        IRepository repo =new Repository(inMemory);
        var user = repo.GetUserByGoogleId("1234");

        Assert.NotNull(user);
        }

         [Fact]
        public void GetUserByGoogleId_shouldReturnNoUser()
        {

          var options = new DbContextOptionsBuilder<EFDataContext>()
                .UseInMemoryDatabase(databaseName: "getPublicationById")
                .Options;
            var inMemory = new EFDataContext(options);

        IRepository repo =new Repository(inMemory);
        var user = repo.GetUserByGoogleId("4321");

        Assert.Null(user);
        }

        [Fact]
        public void SaveChanges_ShouldSaveChanges()
        {

          var options = new DbContextOptionsBuilder<EFDataContext>()
                .UseInMemoryDatabase(databaseName: "getPublicationById")
                .Options;
            var inMemory = new EFDataContext(options);

        IRepository repo =new Repository(inMemory);
        User user = repo.GetUserByGoogleId("1234");
        var new_name="Otro Nombre";
        user.Name=new_name;
        repo.SaveChanges();
        User user2 = repo.GetUserByGoogleId("1234");

        Assert.Equal(user.Name,new_name);
        }
    }
}