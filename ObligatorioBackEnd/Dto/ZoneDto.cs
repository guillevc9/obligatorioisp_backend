

using ObligatorioBackEnd.Models;

namespace ObligatorioBackEnd.Dto
{
    public class ZoneDto
    {
        public string Name{get;set;}
        public ZoneDto(){}

        public ZoneDto(Zone zone){
            Name=zone.Name;
        }
    }
}