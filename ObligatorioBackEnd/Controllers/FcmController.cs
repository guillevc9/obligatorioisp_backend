using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using ObligatorioBackEnd.DataContext;
using ObligatorioBackEnd.Handlers;
using ObligatorioBackEnd.Models;


namespace ObligatorioBackEnd.Controllers
{
    [Route("api/firebase")]
    [ApiController]
    public class FcmController : ControllerBase
    {
        private IRepository _repository;
        private ITokenHandler _tokenHandler = new TokenHandler();

        public FcmController(IRepository repository, ITokenHandler tokenHandler){
            _repository=repository;
            _tokenHandler=tokenHandler;
        }

        [HttpPost("token")]
        public async Task<IActionResult> AddFcmToken([FromBody] string fcm_token)
        {
            Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: AddFcmToken");

            string authToken = Request.Headers[HeaderNames.Authorization];

            Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: AddFcmToken - Google_Token: {authToken}");
            Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: AddFcmToken - Fcm_Token: {fcm_token}");

            var validateToken = await _tokenHandler.IsValid(authToken);

            if (validateToken)
            {
                Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: AddFcmToken - Fcm_Token: User token valido");
                var user = _repository.GetUserByGoogleId(_tokenHandler.GetGoogleId(authToken));
                user.Fcm_token = fcm_token;
                _repository.SaveChanges();
                return Ok();
            }
            else
            {
                return Unauthorized();
            }
        }
    }
}