﻿using System;
using System.Collections.Generic;

namespace ObligatorioBackEnd
{
    public class TokenMap
    {
        private readonly static TokenMap _instance = new TokenMap();

        private Dictionary<string, string> Tokens_obtenidos { get; set; }

        private TokenMap()
        {
            Tokens_obtenidos = new Dictionary<string, string>();
        }

        public static TokenMap Instance
        {
            get { return _instance; }
        }

        public Boolean Exists(string token)
        {
            return Tokens_obtenidos.ContainsValue(token);
        }

        public void Add(string id_google, string token)
        {
            if (Tokens_obtenidos.ContainsKey(id_google))
            {
                Tokens_obtenidos[id_google] = token;
            }
            else
            {
                Tokens_obtenidos.Add(id_google, token);
            }
        }

    }
}
