using System.Threading.Tasks;
using ObligatorioBackEnd.Models;

namespace ObligatorioBackEnd.Handlers
{
    public interface ITokenHandler
    {
         string GetGoogleId(string token);
         Task<bool> IsValid(string token);
         User GetUser(string token);
    }
}