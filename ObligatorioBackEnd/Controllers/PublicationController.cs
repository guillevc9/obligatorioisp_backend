﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using ObligatorioBackEnd.DataContext;
using ObligatorioBackEnd.Dto;
using ObligatorioBackEnd.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Net.Http.Headers;
using ObligatorioBackEnd.Handlers;
using System.ComponentModel.DataAnnotations;

namespace ObligatorioBackEnd.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class PublicationController : ControllerBase
    {
        ITokenHandler _tokenHandler;
        IRepository _repository;
        private readonly IHostingEnvironment _hostingEnvironment;

        public PublicationController(IHostingEnvironment hostingEnvironment, IRepository repository, ITokenHandler tokenHandler)
        {
            _hostingEnvironment = hostingEnvironment;
            _repository = repository;
            _tokenHandler = tokenHandler;
        }

        [HttpPost("{id}/question")]
        public async Task<IActionResult> AddQuestion(int id, [FromBody] QuestionDto question)
        {
            Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: AddQuestion");
            Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: AddQuestion: Question {question.Question} - {question.Answer}");
            String authToken = Request.Headers[HeaderNames.Authorization];
            var validateToken = await _tokenHandler.IsValid(authToken);

            if (validateToken)
            {
                Question quest = new Question(question);
                var pub = _repository.GetPublicationById(id);

                if (pub != null && pub.Active)
                {
                    var context = new ValidationContext(quest, serviceProvider: null, items: null);
                    var validationResults = new List<ValidationResult>();
                    bool validQuestion = Validator.TryValidateObject(quest, context, validationResults, true);

                    if (validQuestion)
                    {
                        Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: AddQuestion: Pub OK");
                        pub.Questions.Add(quest);
                        _repository.SaveChanges();

                        PushNotification pushNotification = new PushNotification(_repository);
                        pushNotification.SendNewQuestionNotification(pub.User.Id_Google);

                        return Ok();
                    }
                    else
                    {
                        return BadRequest();
                    }
                }
                else
                {
                    Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: AddQuestion: No encuento PUB");
                    return NotFound();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [HttpGet("{id}/questions")]
        public async Task<IActionResult> GetQuestions(int id)
        {
            Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: GetQuestions");
            String authToken = Request.Headers[HeaderNames.Authorization];
            var validateToken = await _tokenHandler.IsValid(authToken);

            if (validateToken)
            {
                var questions = new List<QuestionDto>();

                foreach (var item in _repository.GetQuestionsByPublication(id))
                {
                    questions.Add(new QuestionDto(item));
                }
                return Ok(questions);
            }
            else
            {
                return Unauthorized();
            }
        }

        [HttpPost("{idPub}/question/answer")]
        public async Task<IActionResult> AnswerQuestion(int idPub, [FromBody] QuestionDto questionDto)
        {
            Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: AnswerQuestion");
            String authToken = Request.Headers[HeaderNames.Authorization];
            var validateToken = await _tokenHandler.IsValid(authToken);

            if (validateToken)
            {
                var pub = _repository.GetPublicationById(idPub);
                if (pub != null && pub.Active)
                {
                    Question question = _repository.GetQuestionByPublication(idPub, questionDto.QuestionId);
                    if (question != null)
                    {
                        Question quest = new Question(questionDto);
                        var context = new ValidationContext(quest, serviceProvider: null, items: null);
                        var validationResults = new List<ValidationResult>();
                        bool validQuestion = Validator.TryValidateObject(quest, context, validationResults, true);

                        if (validQuestion && questionDto.Answer.Trim() != "")
                        {
                            question.Answer = questionDto.Answer;
                            _repository.SaveChanges();
                            return Ok(new QuestionDto(question));
                        }
                        else
                        {
                            return BadRequest();
                        }
                    }
                    else
                    {
                        return NotFound();
                    }
                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                return Unauthorized();
            }
        }


        [HttpGet]
        public IActionResult GetPublications([FromQuery] string category, [FromQuery] string zone)
        {
            Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: GetPublications");
            var pubsDto = new List<PublicationDTO>();
            foreach (var item in _repository.GetFilterPublications(category, zone))
            {
                pubsDto.Add(new PublicationDTO(item));
            }
            return Ok(pubsDto);
        }

        [HttpPost]
        public async Task<IActionResult> AddPublication([FromBody] PublicationDTO pubDTO)
        {
            Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: AddPublication");
            String authToken = Request.Headers[HeaderNames.Authorization];
            var validateToken = await _tokenHandler.IsValid(authToken);

            if (validateToken)
            {
                Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: AddPublication - Token valido");

                Publication pub = new Publication(pubDTO);
                Category cat = _repository.GetCategoryByName(pubDTO.Category);
                User usr = _repository.GetUserByGoogleId(_tokenHandler.GetGoogleId(authToken));
                Zone zne = _repository.GetZoneByName(pubDTO.Zone);

                pub.User = usr;
                pub.Category = cat;
                pub.Zone = zne;
                pub.Active = true;

                var context = new ValidationContext(pub, serviceProvider: null, items: null);
                var validationResults = new List<ValidationResult>();
                bool valid = Validator.TryValidateObject(pub, context, validationResults, true);

                if (valid)
                {
                    _repository.AddPublication(pub);

                    Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: AddPublication - Publicacion guardada" + pub.PublicationId);
                    return Created(new Uri("/api/publicacion", UriKind.Relative), new PublicationDTO(pub));
                }
                else
                {
                    Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: AddPublication - Modelo no valido");
                    return BadRequest();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [HttpPost("photo")]
        public async Task<IActionResult> AddPhotoToPublication([FromForm] PublicationPhotosDto pubPhotosDto)
        {
            //TODO Falta validar el token
            Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: AddPhotoToPublication");
            Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: AddPhotoToPublication - Cantidad de fotos: {pubPhotosDto.Photos.Count}");
            Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: AddPhotoToPublication - ID de publication: {pubPhotosDto.idPublication}");
            var request = Request;
            var images = Path.Combine(_hostingEnvironment.ContentRootPath, "images");


            String authToken = Request.Headers[HeaderNames.Authorization];
            var validateToken = await _tokenHandler.IsValid(authToken);

            if (validateToken)
            {
                int pos = 1;

                string[] files = System.IO.Directory.GetFiles(images, $"{pubPhotosDto.idPublication}-*.png", System.IO.SearchOption.TopDirectoryOnly);
                Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: AddPhotoToPublication: Cantidad de archivos: " + files.Length);

                string filePathCheck;
                for (int i = 1; i <= 3; i++)
                {
                    filePathCheck = Path.Combine(images, String.Format("{0}-{1}.{2}", pubPhotosDto.idPublication, pos, "png"));
                    if (System.IO.File.Exists(filePathCheck))
                    {
                        pos++;
                    }
                }
                Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: AddPhotoToPublication: Posición donde comienza a grabar: " + pos);
                int photos = pubPhotosDto.Photos.Count;
                int available = 4 - pos;

                if (available >= photos)
                {
                    foreach (var photo in pubPhotosDto.Photos)
                    {
                        if (photo.Length > 0)
                        {
                            string ext = photo.FileName.Split('.')[1];
                            var filePath = Path.Combine(images, String.Format("{0}-{1}.{2}", pubPhotosDto.idPublication, pos, ext));
                            using (var fileStream = new FileStream(filePath, FileMode.Create))
                            {
                                photo.CopyTo(fileStream);
                            }
                        }
                        pos++;
                    }
                    return Ok(new { count = pubPhotosDto.Photos.Count });
                }
                else
                {
                    //Hay mas fotos que disponilidad
                    return BadRequest();
                }
            }
            else
            {
                return Unauthorized();
            }
        }


        [HttpGet("{idPub}/downloadPhoto/{idPhoto}")]
        public async Task<IActionResult> DownloadPhoto(int idPub, int idPhoto)
        {
            Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: DownloadPhoto");

            String authToken = Request.Headers[HeaderNames.Authorization];
            var validateToken = await _tokenHandler.IsValid(authToken);

            if (validateToken)
            {
                string filePath = Path.Combine(_hostingEnvironment.ContentRootPath, "images");
                string fileName = $"{idPub}-{idPhoto}.png";
                //TODO catch de FileNotFoundException
                byte[] fileBytes = System.IO.File.ReadAllBytes(String.Format("{0}/{1}", filePath, fileName));

                Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: DownloadPhoto - Retorno foto: {fileName}");
                return File(fileBytes, "application/force-download", fileName);
            }
            else
            {
                return Unauthorized();
            }
        }

        [HttpPut]
        public async Task<IActionResult> ModifyPublication([FromBody] PublicationDTO pubDto)
        {
            //TODO Falta chequear que quien hizo la publiacion sea el que la modifique
            Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: ModifyPublication");

            String authToken = Request.Headers[HeaderNames.Authorization];
            var validateToken = await _tokenHandler.IsValid(authToken);

            if (validateToken)
            {
                Publication pub = _repository.GetPublicationById(pubDto.Id);
                if (pub != null)
                {
                    Category cat = _repository.GetCategoryByName(pubDto.Category);
                    Zone zone = _repository.GetZoneByName(pubDto.Zone);

                    pub.Title = pubDto.Title;
                    pub.Description = pubDto.Description;
                    pub.Address = pubDto.Address;
                    pub.Category = cat;
                    pub.Zone = zone;
                    pub.Photos = pubDto.Photos;

                    _repository.SaveChanges();
                    return Ok(new PublicationDTO(pub));
                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetPublicationById(int id)
        {
            Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: GetPublicationById");

            String authToken = Request.Headers[HeaderNames.Authorization];
            var validateToken = await _tokenHandler.IsValid(authToken);

            if (validateToken)
            {
                var pub = _repository.GetPublicationById(id);
                if (pub != null)
                {
                    return Ok(new PublicationDTO(pub));
                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [HttpGet("created")]
        public async Task<IActionResult> GetPublicationsCreated()
        {
            Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: GetPublicationsCreated");
            String authToken = Request.Headers[HeaderNames.Authorization];
            var validateToken = await _tokenHandler.IsValid(authToken);

            if (validateToken)
            {
                var pubsDto = new List<PublicationDTO>();
                foreach (var item in _repository.GetPublicationsCreatedByUser(_tokenHandler.GetGoogleId(authToken)))
                {
                    pubsDto.Add(new PublicationDTO(item));
                }
                return Ok(pubsDto);
            }
            else
            {
                return Unauthorized();
            }
        }

        [HttpGet("{id}/annotate")]
        public async Task<IActionResult> AnnotateUser(int id)
        {
            Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: AnnotateUser");
            String authToken = Request.Headers[HeaderNames.Authorization];
            var validateToken = await _tokenHandler.IsValid(authToken);

            if (validateToken)
            {
                Publication pub = _repository.GetPublicationById(id);
                if (pub != null && pub.Active)
                {
                    var isAnnotated = _repository.isUserAnnotatedInPublication(id, _tokenHandler.GetGoogleId(authToken));
                    if (!isAnnotated)
                    {
                        User user = _repository.GetUserByGoogleId(_tokenHandler.GetGoogleId(authToken));
                        //TODO Capaz que esto lo tendria que hacer Repository
                        pub.AnnotatedUsers.Add(new AnnotatedUsers(pub, user));
                        _repository.SaveChanges();
                        return Ok(true);
                    }
                    else
                    {
                        return BadRequest(false);
                    }
                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [HttpGet("{id}/discard")]
        public async Task<IActionResult> DiscardUser(int id)
        {
            Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: DiscardUser");
            String authToken = Request.Headers[HeaderNames.Authorization];
            var validateToken = await _tokenHandler.IsValid(authToken);

            if (validateToken)
            {
                Publication pub = _repository.GetPublicationById(id);
                if (pub != null && pub.Active)
                {
                    var isAnnotated = _repository.isUserAnnotatedInPublication(id, _tokenHandler.GetGoogleId(authToken));
                    if (isAnnotated)
                    {
                        User user = _repository.GetUserByGoogleId(_tokenHandler.GetGoogleId(authToken));
                        //TODO Capaz que esto lo tendria que hacer Repository
                        AnnotatedUsers annotatedUser = pub.AnnotatedUsers.Where(au => au.UserId == user.UserId).Single();
                        pub.AnnotatedUsers.Remove(annotatedUser);
                        _repository.SaveChanges();
                        Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: DiscardUser - Se desanoto usuario");
                        return Ok(true);
                    }
                    else
                    {
                        return BadRequest(false);
                    }
                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [HttpGet("annotated/{id}")]
        public async Task<IActionResult> CheckUserIsAnnotated(int id)
        {

            Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: CheckUserIsAnnotated");
            String authToken = Request.Headers[HeaderNames.Authorization];
            var validateToken = await _tokenHandler.IsValid(authToken);

            if (validateToken)
            {
                if (_repository.GetPublicationById(id) != null)
                {
                    if (_repository.isUserAnnotatedInPublication(id, _tokenHandler.GetGoogleId(authToken)))
                    {
                        Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: CheckUserIsAnnotated - El usuario esta anotado");
                        return Ok(true);
                    }
                    else
                    {
                        Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: CheckUserIsAnnotated - El usuario no esta anotado");
                        return Ok(false);
                    }
                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [HttpGet("annotated")]
        public async Task<IActionResult> GetAnnotatedPublications()
        {
            Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: GetAnnotatedPublications");
            String authToken = Request.Headers[HeaderNames.Authorization];
            var validateToken = await _tokenHandler.IsValid(authToken);

            if (validateToken)
            {
                var pubsDto = new List<PublicationDTO>();
                foreach (var item in _repository.GetPublicationsAnnotatedByUser(_tokenHandler.GetGoogleId(authToken)))
                {
                    pubsDto.Add(new PublicationDTO(item));
                }
                return Ok(pubsDto);
            }
            else
            {
                return Unauthorized();
            }
        }

        [HttpGet("{id}/annotatedUsers")]
        public async Task<IActionResult> GetAnnotatedUsers(int id)
        {

            Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: GetAnnotatedUsers");
            String authToken = Request.Headers[HeaderNames.Authorization];
            var validateToken = await _tokenHandler.IsValid(authToken);

            if (validateToken)
            {
                if (_repository.GetPublicationById(id) != null)
                {
                    var users = new List<UserDto>();
                    foreach (var item in _repository.GetAnnotatedUsersByPublication(id))
                    {
                        users.Add(new UserDto(item));
                    }
                    return Ok(users);
                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [HttpPost("finalize")]
        public async Task<IActionResult> FinalizePublication([FromBody] FinalizePublicationDto finalizePubDto)
        {
            Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: FinishedPublication");


            Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: FinalizePublication");
            String authToken = Request.Headers[HeaderNames.Authorization];
            var validateToken = await _tokenHandler.IsValid(authToken);

            if (validateToken)
            {
                var pub = _repository.GetPublicationById(finalizePubDto.IdPublication);
                if (pub != null && pub.Active)
                {
                    FinalizePublication finPub = new FinalizePublication();
                    finPub.Reason = finalizePubDto.Reason;
                    finPub.OtherReason = finalizePubDto.OtherReason;

                    if (finalizePubDto.IdGoogleUser == "")
                    {
                        finPub.UserAssigned = null;
                    }
                    else
                    {
                        var user = _repository.GetUserByGoogleId(finalizePubDto.IdGoogleUser);
                        if (user != null)
                        {
                            finPub.UserAssigned = user;
                            PushNotification pushNotification = new PushNotification(_repository);
                            pushNotification.SendSelectedCandidateNotification(user.Id_Google);
                        }
                        else
                        {
                            return BadRequest();
                        }
                    }
                    pub.FinalizeReason = finPub;
                    pub.Active = false;
                    _repository.SaveChanges();
                    return Ok(true);
                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                return Unauthorized();
            }
        }
    }
}
