using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ObligatorioBackEnd.DataContext;
using ObligatorioBackEnd.Dto;
using ObligatorioBackEnd.Models;

namespace ObligatorioBackEnd.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class ZoneController:ControllerBase
    {
        private  IRepository _repository;
        public ZoneController(IRepository repository)
        {
            _repository=repository;
        }
        
        [HttpGet]
        public IActionResult GetAllZones(){
            var listZonesDto = new List<ZoneDto>();
            foreach(Zone zone in _repository.GetAllZones()){
                listZonesDto.Add(new ZoneDto(zone));
            }
            return Ok(listZonesDto);
        }
    }
}