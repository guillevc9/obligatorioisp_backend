using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ObligatorioBackEnd.Controllers;
using ObligatorioBackEnd.DataContext;
using ObligatorioBackEnd.Dto;
using ObligatorioBackEnd.Models;
using Xunit;

namespace ObligatorioBackend.Tests
{
    public class ZoneControllerTest
    {
         [Fact]
        public void GetAllZones_ShouldListNoZones()
        {
            var options = new DbContextOptionsBuilder<EFDataContext>()
                .UseInMemoryDatabase(databaseName: "RepositoryTest_NoPublications")
                .Options;

            IRepository repo = new Repository(new EFDataContext(options));

            var controller = new ZoneController(repo);
            var result = Assert.IsType<OkObjectResult>(controller.GetAllZones());
            //var model = Assert.IsType<List<CategoryDto>>(result.Value);
            var listZones = result.Value as List<ZoneDto>;
            Assert.Empty(listZones);
        }

        [Fact]
        public void GetAllZones_ShouldListAllCategories()
        {
            var options = new DbContextOptionsBuilder<EFDataContext>()
                           .UseInMemoryDatabase(databaseName: "RepositoryTest_NoPublications")
                           .Options;

            var inMemoryDb = new EFDataContext(options);

            Zone zone1 = new Zone { Name = "Zone1" };
            Zone zone2 = new Zone { Name = "Zone2" };
            Zone zone3 = new Zone { Name = "Zone3" };
            inMemoryDb.Zones.Add(zone1);
            inMemoryDb.Zones.Add(zone2);
            inMemoryDb.Zones.Add(zone3);
            inMemoryDb.SaveChanges();

            IRepository repo = new Repository(inMemoryDb);

            var controller = new ZoneController(repo);

            var result = Assert.IsType<OkObjectResult>(controller.GetAllZones());

            //var model = Assert.IsType<List<CategoryDto>>(result.Value);
            var listZones = result.Value as List<ZoneDto>;
            Assert.Equal<int>(3, listZones.Count);
        }
    }
}