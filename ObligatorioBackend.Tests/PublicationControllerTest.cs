using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using ObligatorioBackEnd.Controllers;
using ObligatorioBackEnd.DataContext;
using ObligatorioBackEnd.Dto;
using ObligatorioBackEnd.Handlers;
using ObligatorioBackEnd.Models;
using Xunit;
using Xunit.Abstractions;
using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;
using System.ComponentModel.DataAnnotations;

namespace ObligatorioBackend.Tests
{
    public class PublicationControllerTest : IDisposable
    {
        EFDataContext inMemory;
        ITestOutputHelper output;
        public PublicationControllerTest(ITestOutputHelper output)
        {
            this.output = output;
            var options = new DbContextOptionsBuilder<EFDataContext>()
              .UseInMemoryDatabase(databaseName: "PublicationControllerTest")
              .Options;

            inMemory = new EFDataContext(options);

            User usr = new User
            {
                Name = "Usuario",
                LastName = "De Prueba",
                Mail = "usuario@deprueba.com",
                Id_Google = "1234",
                Fcm_token = "1234",
                AnnotatedPublications = new List<AnnotatedUsers>()
            };

            Category cat = new Category
            {
                Name = "Categoria1"
            };

            Zone zon = new Zone
            {
                Name = "Zone1"
            };

            inMemory.Users.Add(usr);
            inMemory.Categories.Add(cat);
            inMemory.Zones.Add(zon);
            inMemory.SaveChanges();
        }

        [Fact]
        public async void AddPublication_ShouldAddPublication()
        {
            IRepository _repo = new Repository(inMemory);

            PublicationDTO pubDto = new PublicationDTO
            {
                Active = true,
                Title = "Un titulo",
                Description = "Una descripcion",
                Address = "Una direccion",
                Category = "Categoria1",
                Zone = "Zone1",
                UserId = "1234",
                UserName = "",
                UserFamilyName = "",
                Reason = "",
                UserAssignedId = "",
                UserAssigned = "",
                OtherReason = "",
                Photos = 0,
                Id = 0,
            };

            var mockEnvironment = new Mock<IHostingEnvironment>();

            mockEnvironment
                .Setup(m => m.EnvironmentName)
                .Returns("Hosting:UnitTestEnvironment");

            var mockTokenHandler = new Mock<ITokenHandler>();
            mockTokenHandler
                .Setup(th => th.IsValid(It.IsAny<string>())).ReturnsAsync(true);
            mockTokenHandler
                .Setup(th => th.GetGoogleId(It.IsAny<string>())).Returns("1234");

            PublicationController controller = new PublicationController(mockEnvironment.Object, _repo, mockTokenHandler.Object);

            //Fake Request.Header
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Request.Headers[HeaderNames.Authorization] = "1234";

            Assert.IsType<CreatedResult>(await controller.AddPublication(pubDto));

        }

        [Fact]
        public async void AddPublication_ShouldNotAuthorizeUser()
        {
            var mockTokenHandler = new Mock<ITokenHandler>();
            mockTokenHandler
                .Setup(th => th.IsValid(It.IsAny<string>())).ReturnsAsync(false);

            PublicationController controller = new PublicationController(null, null, mockTokenHandler.Object);

            //Fake Request.Header
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Request.Headers[HeaderNames.Authorization] = "1234";

            Assert.IsType<UnauthorizedResult>(await controller.AddPublication(new PublicationDTO()));

        }

        [Fact]
        public void AddPublication_ShouldNotAddPublicationWithEmptyTitle()

        {
            IRepository _repo = new Repository(inMemory);

            PublicationDTO pubDto = new PublicationDTO
            {
                Active = true,
                Title = "",
                Description = "Una descripcion",
                Address = "Una direccion",
                Category = "Categoria1",
                Zone = "Zone1",
                UserId = "1234",
                UserName = "",
                UserFamilyName = "",
                Reason = "",
                UserAssignedId = "",
                UserAssigned = "",
                OtherReason = "",
                Photos = 0,
                Id = 0,
            };

            Publication pub = new Publication(pubDto);
            Category cat = _repo.GetCategoryByName(pubDto.Category);
            User usr = _repo.GetUserByGoogleId("1234");
            Zone zne = _repo.GetZoneByName(pubDto.Zone);

            pub.User = usr;
            pub.Category = cat;
            pub.Zone = zne;
            pub.Active = true;

            var validationResultList = new List<ValidationResult>();

            bool validate = Validator.TryValidateObject(pub, new ValidationContext(pub), validationResultList);
            Assert.False(validate);
        }

        [Fact]
        public async void AddPublication_ShouldNotAddPublicationWithMoreThan40CharactersInTitle()

        {
            IRepository _repo = new Repository(inMemory);

            PublicationDTO pubDto = new PublicationDTO
            {
                Active = true,
                Title = "1234567890123456789012345678901234567890123456789012345678901234567890",
                Description = "Una descripcion",
                Address = "Una direccion",
                Category = "Categoria1",
                Zone = "Zone1",
                UserId = "1234",
                UserName = "",
                UserFamilyName = "",
                Reason = "",
                UserAssignedId = "",
                UserAssigned = "",
                OtherReason = "",
                Photos = 0,
                Id = 0,
            };

            var mockEnvironment = new Mock<IHostingEnvironment>();
            mockEnvironment
                .Setup(m => m.EnvironmentName)
                .Returns("Hosting:UnitTestEnvironment");

            var mockTokenHandler = new Mock<ITokenHandler>();
            mockTokenHandler
                .Setup(th => th.IsValid(It.IsAny<string>())).ReturnsAsync(true);
            mockTokenHandler
                .Setup(th => th.GetGoogleId(It.IsAny<string>())).Returns("1234");

            PublicationController controller = new PublicationController(mockEnvironment.Object, _repo, mockTokenHandler.Object);

            //Fake Request.Header
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Request.Headers[HeaderNames.Authorization] = "1234";

            Assert.IsType<BadRequestResult>(await controller.AddPublication(pubDto));
        }

        [Fact]
        public async void AddPublication_ShouldNotAddPublicationWithMoreThan200CharactersInDescription()
        {
            IRepository _repo = new Repository(inMemory);

            PublicationDTO pubDto = new PublicationDTO
            {
                Active = true,
                Title = "Un titulo",
                Description = "123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901",
                Address = "Una direccion",
                Category = "Categoria1",
                Zone = "Zone1",
                UserId = "1234",
                UserName = "",
                UserFamilyName = "",
                Reason = "",
                UserAssignedId = "",
                UserAssigned = "",
                OtherReason = "",
                Photos = 0,
                Id = 0,
            };

            var mockEnvironment = new Mock<IHostingEnvironment>();
            mockEnvironment
                .Setup(m => m.EnvironmentName)
                .Returns("Hosting:UnitTestEnvironment");

            var mockTokenHandler = new Mock<ITokenHandler>();
            mockTokenHandler
                .Setup(th => th.IsValid(It.IsAny<string>())).ReturnsAsync(true);
            mockTokenHandler
                .Setup(th => th.GetGoogleId(It.IsAny<string>())).Returns("1234");

            PublicationController controller = new PublicationController(mockEnvironment.Object, _repo, mockTokenHandler.Object);

            //Fake Request.Header
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Request.Headers[HeaderNames.Authorization] = "1234";

            Assert.IsType<BadRequestResult>(await controller.AddPublication(pubDto));
        }

        [Fact]
        public async void AddPublication_ShouldNotAddPublicationWithNoAddress()
        {
            IRepository _repo = new Repository(inMemory);

            PublicationDTO pubDto = new PublicationDTO
            {
                Active = true,
                Title = "Un titulo",
                Description = "123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901",
                Address = "",
                Category = "Categoria1",
                Zone = "Zone1",
                UserId = "1234",
                UserName = "",
                UserFamilyName = "",
                Reason = "",
                UserAssignedId = "",
                UserAssigned = "",
                OtherReason = "",
                Photos = 0,
                Id = 0,
            };

            var mockEnvironment = new Mock<IHostingEnvironment>();
            mockEnvironment
                .Setup(m => m.EnvironmentName)
                .Returns("Hosting:UnitTestEnvironment");

            var mockTokenHandler = new Mock<ITokenHandler>();
            mockTokenHandler
                .Setup(th => th.IsValid(It.IsAny<string>())).ReturnsAsync(true);
            mockTokenHandler
                .Setup(th => th.GetGoogleId(It.IsAny<string>())).Returns("1234");

            PublicationController controller = new PublicationController(mockEnvironment.Object, _repo, mockTokenHandler.Object);

            //Fake Request.Header
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Request.Headers[HeaderNames.Authorization] = "1234";

            Assert.IsType<BadRequestResult>(await controller.AddPublication(pubDto));
        }

        [Fact]
        public async void AddQuestion_ShouldAddQuestion()
        {
            IRepository _repo = new Repository(inMemory);

            PublicationDTO pubDto = new PublicationDTO
            {
                Active = true,
                Title = "Un titulo",
                Description = "Una descripcion",
                Address = "Una direccion",
                Category = "Categoria1",
                Zone = "Zone1",
                UserId = "1234",
                UserName = "",
                UserFamilyName = "",
                Reason = "",
                UserAssignedId = "",
                UserAssigned = "",
                OtherReason = "",
                Photos = 0,
                Id = 0,
            };

            Publication pub = new Publication(pubDto);
            Category cat = _repo.GetCategoryByName(pubDto.Category);
            User usr = _repo.GetUserByGoogleId("1234");
            Zone zne = _repo.GetZoneByName(pubDto.Zone);

            pub.User = usr;
            pub.Category = cat;
            pub.Zone = zne;
            pub.Active = true;

            inMemory.Publications.Add(pub);
            inMemory.SaveChanges();

            var mockEnvironment = new Mock<IHostingEnvironment>();

            mockEnvironment
                .Setup(m => m.EnvironmentName)
                .Returns("Hosting:UnitTestEnvironment");

            var mockTokenHandler = new Mock<ITokenHandler>();
            mockTokenHandler
                .Setup(th => th.IsValid(It.IsAny<string>())).ReturnsAsync(true);
            mockTokenHandler
                .Setup(th => th.GetGoogleId(It.IsAny<string>())).Returns("1234");

            PublicationController controller = new PublicationController(mockEnvironment.Object, _repo, mockTokenHandler.Object);
            //Fake Request.Header
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Request.Headers[HeaderNames.Authorization] = "1234";
            QuestionDto questDto = new QuestionDto();
            questDto.Question = "Esta es una pregunta";
            questDto.Answer = "";
            
            Assert.IsType<OkResult>(await controller.AddQuestion(pub.PublicationId, questDto));
        }

        [Fact]
        public async void AddQuestion_ShouldNotAuthorizeUser()
        {
            IRepository _repo = new Repository(inMemory);

            PublicationDTO pubDto = new PublicationDTO
            {
                Active = true,
                Title = "Un titulo",
                Description = "Una descripcion",
                Address = "Una direccion",
                Category = "Categoria1",
                Zone = "Zone1",
                UserId = "1234",
                UserName = "",
                UserFamilyName = "",
                Reason = "",
                UserAssignedId = "",
                UserAssigned = "",
                OtherReason = "",
                Photos = 0,
                Id = 0,
            };

            Publication pub = new Publication(pubDto);
            Category cat = _repo.GetCategoryByName(pubDto.Category);
            User usr = _repo.GetUserByGoogleId("1234");
            Zone zne = _repo.GetZoneByName(pubDto.Zone);

            pub.User = usr;
            pub.Category = cat;
            pub.Zone = zne;
            pub.Active = true;

            inMemory.Publications.Add(pub);
            inMemory.SaveChanges();

            var mockEnvironment = new Mock<IHostingEnvironment>();

            mockEnvironment
                .Setup(m => m.EnvironmentName)
                .Returns("Hosting:UnitTestEnvironment");

            var mockTokenHandler = new Mock<ITokenHandler>();
            mockTokenHandler
                .Setup(th => th.IsValid(It.IsAny<string>())).ReturnsAsync(false);
            mockTokenHandler
                .Setup(th => th.GetGoogleId(It.IsAny<string>())).Returns("1234");

            PublicationController controller = new PublicationController(mockEnvironment.Object, _repo, mockTokenHandler.Object);
            //Fake Request.Header
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Request.Headers[HeaderNames.Authorization] = "1234";
            QuestionDto questDto = new QuestionDto();
            questDto.Question = "Esta es una pregunta";
            questDto.Answer = "";

            Assert.IsType<UnauthorizedResult>(await controller.AddQuestion(pub.PublicationId, questDto));
        }

        [Fact]
        public async void AddQuestion_ShouldNotAddQuestionWithEmptyText()
        {
            IRepository _repo = new Repository(inMemory);

            PublicationDTO pubDto = new PublicationDTO
            {
                Active = true,
                Title = "Un titulo",
                Description = "Una descripcion",
                Address = "Una direccion",
                Category = "Categoria1",
                Zone = "Zone1",
                UserId = "1234",
                UserName = "",
                UserFamilyName = "",
                Reason = "",
                UserAssignedId = "",
                UserAssigned = "",
                OtherReason = "",
                Photos = 0,
                Id = 0,
            };

            Publication pub = new Publication(pubDto);
            Category cat = _repo.GetCategoryByName(pubDto.Category);
            User usr = _repo.GetUserByGoogleId("1234");
            Zone zne = _repo.GetZoneByName(pubDto.Zone);

            pub.User = usr;
            pub.Category = cat;
            pub.Zone = zne;
            pub.Active = true;

            inMemory.Publications.Add(pub);
            inMemory.SaveChanges();

            var mockEnvironment = new Mock<IHostingEnvironment>();

            mockEnvironment
                .Setup(m => m.EnvironmentName)
                .Returns("Hosting:UnitTestEnvironment");

            var mockTokenHandler = new Mock<ITokenHandler>();
            mockTokenHandler
                .Setup(th => th.IsValid(It.IsAny<string>())).ReturnsAsync(true);
            mockTokenHandler
                .Setup(th => th.GetGoogleId(It.IsAny<string>())).Returns("1234");

            PublicationController controller = new PublicationController(mockEnvironment.Object, _repo, mockTokenHandler.Object);
            //Fake Request.Header
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Request.Headers[HeaderNames.Authorization] = "1234";
            QuestionDto questDto = new QuestionDto();
            questDto.Question = "";
            questDto.Answer = "";

            Assert.IsType<BadRequestResult>(await controller.AddQuestion(pub.PublicationId, questDto));
        }

        [Fact]
        public async void AddQuestion_ShouldNotAddQuestionWithMoreThan140Characters()
        {
            IRepository _repo = new Repository(inMemory);

            PublicationDTO pubDto = new PublicationDTO
            {
                Active = true,
                Title = "Un titulo",
                Description = "Una descripcion",
                Address = "Una direccion",
                Category = "Categoria1",
                Zone = "Zone1",
                UserId = "1234",
                UserName = "",
                UserFamilyName = "",
                Reason = "",
                UserAssignedId = "",
                UserAssigned = "",
                OtherReason = "",
                Photos = 0,
                Id = 0,
            };

            Publication pub = new Publication(pubDto);
            Category cat = _repo.GetCategoryByName(pubDto.Category);
            User usr = _repo.GetUserByGoogleId("1234");
            Zone zne = _repo.GetZoneByName(pubDto.Zone);

            pub.User = usr;
            pub.Category = cat;
            pub.Zone = zne;
            pub.Active = true;

            inMemory.Publications.Add(pub);
            inMemory.SaveChanges();

            var mockEnvironment = new Mock<IHostingEnvironment>();

            mockEnvironment
                .Setup(m => m.EnvironmentName)
                .Returns("Hosting:UnitTestEnvironment");

            var mockTokenHandler = new Mock<ITokenHandler>();
            mockTokenHandler
                .Setup(th => th.IsValid(It.IsAny<string>())).ReturnsAsync(true);
            mockTokenHandler
                .Setup(th => th.GetGoogleId(It.IsAny<string>())).Returns("1234");

            PublicationController controller = new PublicationController(mockEnvironment.Object, _repo, mockTokenHandler.Object);
            //Fake Request.Header
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Request.Headers[HeaderNames.Authorization] = "1234";
            QuestionDto questDto = new QuestionDto();
            questDto.Question = "123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890";
            questDto.Answer = "";

            Assert.IsType<BadRequestResult>(await controller.AddQuestion(pub.PublicationId, questDto));
        }

        [Fact]
        public async void AnswerQuestion_ShouldAddAnswer()
        {
            IRepository _repo = new Repository(inMemory);

            PublicationDTO pubDto = new PublicationDTO
            {
                Active = true,
                Title = "Un titulo",
                Description = "Una descripcion",
                Address = "Una direccion",
                Category = "Categoria1",
                Zone = "Zone1",
                UserId = "1234",
                UserName = "",
                UserFamilyName = "",
                Reason = "",
                UserAssignedId = "",
                UserAssigned = "",
                OtherReason = "",
                Photos = 0,
                Id = 0,
            };

            Publication pub = new Publication(pubDto);
            Category cat = _repo.GetCategoryByName(pubDto.Category);
            User usr = _repo.GetUserByGoogleId("1234");
            Zone zne = _repo.GetZoneByName(pubDto.Zone);

            pub.User = usr;
            pub.Category = cat;
            pub.Zone = zne;
            pub.Active = true;
            pub.Questions = new List<Question>();

            QuestionDto questionDto = new QuestionDto();
            questionDto.Question = "Una Pregunta";
            questionDto.Answer = "";

            var question = new Question(questionDto);
            pub.Questions.Add(question);

            inMemory.Publications.Add(pub);
            inMemory.SaveChanges();


            var mockEnvironment = new Mock<IHostingEnvironment>();

            mockEnvironment
                .Setup(m => m.EnvironmentName)
                .Returns("Hosting:UnitTestEnvironment");

            var mockTokenHandler = new Mock<ITokenHandler>();
            mockTokenHandler
                .Setup(th => th.IsValid(It.IsAny<string>())).ReturnsAsync(true);
            mockTokenHandler
                .Setup(th => th.GetGoogleId(It.IsAny<string>())).Returns("1234");

            PublicationController controller = new PublicationController(mockEnvironment.Object, _repo, mockTokenHandler.Object);
            //Fake Request.Header
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Request.Headers[HeaderNames.Authorization] = "1234";

            QuestionDto questDto = new QuestionDto();
            questDto.QuestionId = question.Id;
            questDto.Question = "Una pregunta";
            questDto.Answer = "Una respuesta";

            Assert.IsType<OkObjectResult>(await controller.AnswerQuestion(pub.PublicationId, questDto));

        }

        [Fact]
        public async void AnswerQuestion_ShouldNotAuthorizeUSer()
        {
            IRepository _repo = new Repository(inMemory);

            PublicationDTO pubDto = new PublicationDTO
            {
                Active = true,
                Title = "Un titulo",
                Description = "Una descripcion",
                Address = "Una direccion",
                Category = "Categoria1",
                Zone = "Zone1",
                UserId = "1234",
                UserName = "",
                UserFamilyName = "",
                Reason = "",
                UserAssignedId = "",
                UserAssigned = "",
                OtherReason = "",
                Photos = 0,
                Id = 0,
            };

            Publication pub = new Publication(pubDto);
            Category cat = _repo.GetCategoryByName(pubDto.Category);
            User usr = _repo.GetUserByGoogleId("1234");
            Zone zne = _repo.GetZoneByName(pubDto.Zone);

            pub.User = usr;
            pub.Category = cat;
            pub.Zone = zne;
            pub.Active = true;
            pub.Questions = new List<Question>();

            QuestionDto questionDto = new QuestionDto();
            questionDto.Question = "Una Pregunta";
            questionDto.Answer = "";

            var question = new Question(questionDto);
            pub.Questions.Add(question);

            inMemory.Publications.Add(pub);
            inMemory.SaveChanges();


            var mockEnvironment = new Mock<IHostingEnvironment>();

            mockEnvironment
                .Setup(m => m.EnvironmentName)
                .Returns("Hosting:UnitTestEnvironment");

            var mockTokenHandler = new Mock<ITokenHandler>();
            mockTokenHandler
                .Setup(th => th.IsValid(It.IsAny<string>())).ReturnsAsync(false);
            mockTokenHandler
                .Setup(th => th.GetGoogleId(It.IsAny<string>())).Returns("1234");

            PublicationController controller = new PublicationController(mockEnvironment.Object, _repo, mockTokenHandler.Object);
            //Fake Request.Header
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Request.Headers[HeaderNames.Authorization] = "1234";

            QuestionDto questDto = new QuestionDto();
            questDto.QuestionId = question.Id;
            questDto.Question = "Una pregunta";
            questDto.Answer = "Una respuesta";

            Assert.IsType<UnauthorizedResult>(await controller.AnswerQuestion(pub.PublicationId, questDto));
        }

        [Fact]
        public async void AnswerQuestion_ShouldNotAddAnswerWithEmptyAnswer()
        {
            IRepository _repo = new Repository(inMemory);

            PublicationDTO pubDto = new PublicationDTO
            {
                Active = true,
                Title = "Un titulo",
                Description = "Una descripcion",
                Address = "Una direccion",
                Category = "Categoria1",
                Zone = "Zone1",
                UserId = "1234",
                UserName = "",
                UserFamilyName = "",
                Reason = "",
                UserAssignedId = "",
                UserAssigned = "",
                OtherReason = "",
                Photos = 0,
                Id = 0,
            };

            Publication pub = new Publication(pubDto);
            Category cat = _repo.GetCategoryByName(pubDto.Category);
            User usr = _repo.GetUserByGoogleId("1234");
            Zone zne = _repo.GetZoneByName(pubDto.Zone);

            pub.User = usr;
            pub.Category = cat;
            pub.Zone = zne;
            pub.Active = true;
            pub.Questions = new List<Question>();

            QuestionDto questionDto = new QuestionDto();
            questionDto.Question = "Una Pregunta";
            questionDto.Answer = "";

            var question = new Question(questionDto);
            pub.Questions.Add(question);

            inMemory.Publications.Add(pub);
            inMemory.SaveChanges();


            var mockEnvironment = new Mock<IHostingEnvironment>();

            mockEnvironment
                .Setup(m => m.EnvironmentName)
                .Returns("Hosting:UnitTestEnvironment");

            var mockTokenHandler = new Mock<ITokenHandler>();
            mockTokenHandler
                .Setup(th => th.IsValid(It.IsAny<string>())).ReturnsAsync(true);
            mockTokenHandler
                .Setup(th => th.GetGoogleId(It.IsAny<string>())).Returns("1234");

            PublicationController controller = new PublicationController(mockEnvironment.Object, _repo, mockTokenHandler.Object);
            //Fake Request.Header
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Request.Headers[HeaderNames.Authorization] = "1234";

            QuestionDto questDto = new QuestionDto();
            questDto.QuestionId = question.Id;
            questDto.Question = "Una pregunta";
            questDto.Answer = "";

            Assert.IsType<BadRequestResult>(await controller.AnswerQuestion(pub.PublicationId, questDto));
        }

        [Fact]
        public async void AnswerQuestion_ShouldNotAddAnswerWithMoreThan140CharactersInAnswer()
        {
            IRepository _repo = new Repository(inMemory);

            PublicationDTO pubDto = new PublicationDTO
            {
                Active = true,
                Title = "Un titulo",
                Description = "Una descripcion",
                Address = "Una direccion",
                Category = "Categoria1",
                Zone = "Zone1",
                UserId = "1234",
                UserName = "",
                UserFamilyName = "",
                Reason = "",
                UserAssignedId = "",
                UserAssigned = "",
                OtherReason = "",
                Photos = 0,
                Id = 0,
            };

            Publication pub = new Publication(pubDto);
            Category cat = _repo.GetCategoryByName(pubDto.Category);
            User usr = _repo.GetUserByGoogleId("1234");
            Zone zne = _repo.GetZoneByName(pubDto.Zone);

            pub.User = usr;
            pub.Category = cat;
            pub.Zone = zne;
            pub.Active = true;
            pub.Questions = new List<Question>();

            QuestionDto questionDto = new QuestionDto();
            questionDto.Question = "Una Pregunta";
            questionDto.Answer = "";

            var question = new Question(questionDto);
            pub.Questions.Add(question);

            inMemory.Publications.Add(pub);
            inMemory.SaveChanges();


            var mockEnvironment = new Mock<IHostingEnvironment>();

            mockEnvironment
                .Setup(m => m.EnvironmentName)
                .Returns("Hosting:UnitTestEnvironment");

            var mockTokenHandler = new Mock<ITokenHandler>();
            mockTokenHandler
                .Setup(th => th.IsValid(It.IsAny<string>())).ReturnsAsync(true);
            mockTokenHandler
                .Setup(th => th.GetGoogleId(It.IsAny<string>())).Returns("1234");

            PublicationController controller = new PublicationController(mockEnvironment.Object, _repo, mockTokenHandler.Object);
            //Fake Request.Header
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Request.Headers[HeaderNames.Authorization] = "1234";

            QuestionDto questDto = new QuestionDto();
            questDto.QuestionId = question.Id;
            questDto.Question = "Una pregunta";
            questDto.Answer = "123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890";

            Assert.IsType<BadRequestResult>(await controller.AnswerQuestion(pub.PublicationId, questDto));
        }
        public void Dispose()
        {
            inMemory.Database.EnsureDeleted();
        }
    }
}