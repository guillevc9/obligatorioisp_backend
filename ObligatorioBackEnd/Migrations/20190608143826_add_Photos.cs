﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ObligatorioBackEnd.Migrations
{
    public partial class add_Photos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Photos",
                table: "Publicaciones",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Photos",
                table: "Publicaciones");
        }
    }
}
