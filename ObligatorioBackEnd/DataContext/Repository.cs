using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ObligatorioBackEnd.Dto;
using ObligatorioBackEnd.Models;

namespace ObligatorioBackEnd.DataContext
{
    public sealed class Repository : IRepository
    {
        private EFDataContext _dbContext;

        public Repository()
        {
            _dbContext = new EFDataContext();
        }
        public Repository(EFDataContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<Publication> GetAllPublications()
        {
            return _dbContext.Publications
                .Include(p => p.Category)
                .Include(p => p.User)
                .Include(p => p.Zone)
                .Include(p => p.FinalizeReason).ThenInclude(fr => fr.UserAssigned)
                .OrderByDescending(p => p.PublicationId)
                .ToList();
        }

        public List<Publication> GetFilterPublications(string category, string zone)
        {
            UrlQuery urlQuery = new UrlQuery(category, zone);
            string sql = @"SELECT p.* FROM Publications p, Categories c, Zones z WHERE p.CategoryId=c.Id AND p.ZoneId=z.Id";
            //string sql = @"SELECT p.* FROM Publicaciones p, Categorias c, Zones z WHERE p.CategoriaId=c.Id AND p.ZoneId=z.Id AND p.Active=1";
            if (urlQuery.HaveFilter)
            {
                string filterSQL = "";
                if (!string.IsNullOrEmpty(urlQuery.Category))
                {
                     string cleanCategory= CleanString.Clean(urlQuery.Category);
                    if (_dbContext.Categories.Any(c => c.Name == cleanCategory))
                    {
                        filterSQL += $"c.Name='{cleanCategory}'";
                    }
                    else
                    {
                        return new List<Publication>();
                    }
                }
                if (!string.IsNullOrEmpty(urlQuery.Zone))
                {
                     string cleanZone= CleanString.Clean(urlQuery.Zone);
                    if (!string.IsNullOrEmpty(filterSQL))
                    {
                        if (_dbContext.Zones.Any(z => z.Name == cleanZone))
                        {
                            filterSQL += " AND";
                        }
                        else
                        {
                            return new List<Publication>(); ;
                        }
                    }
                    filterSQL += $" z.Name='{cleanZone}'";
                }
                sql += $" AND {filterSQL}";
            }

            return _dbContext.Publications
                .Include(p => p.Category)
                .Include(p => p.User)
                .Include(p => p.Zone)
                .Include(p => p.FinalizeReason).ThenInclude(fr => fr.UserAssigned)
                .FromSql(sql)
                .ToList() //No sacar, sino falla
                .OrderByDescending(p => p.PublicationId)
                .ToList();
        }

        public Category GetCategoryByName(string category)
        {
            return _dbContext.Categories.Where(c => c.Name == category).SingleOrDefault();
        }

        public Zone GetZoneByName(string zone)
        {
            return _dbContext.Zones.Where(z => z.Name == zone).SingleOrDefault();
        }

        public User GetUserByGoogleId(string googleId)
        {
            return _dbContext.Users.Where(u => u.Id_Google == googleId).SingleOrDefault();
        }

        public int AddPublication(Publication pub)
        {
            _dbContext.Publications.Add(pub);
            return _dbContext.SaveChanges();
        }

        public Publication GetPublicationById(int id)
        {
            var pub = _dbContext.Publications
                    .Include(p => p.User)
                    .Include(p => p.Category)
                    .Include(p => p.Zone)
                    .Include(p => p.AnnotatedUsers)
                    .Include(p => p.Questions)
                    .Where(p => p.PublicationId == id && p.Active == true)
                    .SingleOrDefault();

            if (pub != null)
            {
                return pub;
            }
            else
            {
                pub = _dbContext.Publications
                    .Include(p => p.User)
                    .Include(p => p.Category)
                    .Include(p => p.Zone)
                    .Include(p => p.AnnotatedUsers)
                    .Include(p => p.Questions)
                    .Include(p => p.FinalizeReason).ThenInclude(fp => fp.UserAssigned)
                    .Where(p => p.PublicationId == id && p.Active == false)
                    .SingleOrDefault();
                return pub;
            }
        }

        public int SaveChanges()
        {
            return _dbContext.SaveChanges();
        }

        public List<Publication> GetPublicationsCreatedByUser(string googleId)
        {
            return _dbContext.Publications
              .Include(p => p.User)
              .Include(p => p.Category)
              .Include(p => p.Zone)
              .Include(p => p.FinalizeReason).ThenInclude(fr => fr.UserAssigned)
              .Where(p => p.User.Id_Google == googleId)
              .ToList();
        }

        public List<Category> GetAllCategories()
        {
            return _dbContext.Categories.ToList();
        }

        public List<Zone> GetAllZones()
        {
            return _dbContext.Zones.ToList();
        }

        public int AddUser(User user)
        {
            _dbContext.Users.Add(user);
            return _dbContext.SaveChanges();
        }

        public List<Question> GetQuestionsByPublication(int publicationId)
        {
            return _dbContext.Publications
                .Include(p => p.Questions)
                .Where(p => p.PublicationId == publicationId)
                .SingleOrDefault().Questions.ToList();
        }

        public Question GetQuestionByPublication(int publicationId, int questionId)
        {
            return _dbContext.Publications
               .Include(p => p.Questions)
               .Where(p => p.PublicationId == publicationId)
               .Single().Questions
                   .Where(q => q.Id == questionId)
                   .SingleOrDefault();
        }

        public bool isUserAnnotatedInPublication(int publicationId, string googleId)
        {
            var pub = _dbContext.Publications
                .Include(p => p.AnnotatedUsers)
                .Where(p => p.PublicationId == publicationId).SingleOrDefault();
            var user = _dbContext.Users.Where(u => u.Id_Google == googleId).SingleOrDefault();

            return pub.AnnotatedUsers.Any(au => au.UserId == user.UserId);
        }

        public List<Publication> GetPublicationsAnnotatedByUser(string googleId)
        {
            return _dbContext.Users
                                    .Include(u => u.AnnotatedPublications).ThenInclude(au => au.Publication).ThenInclude(p => p.Category)
                                    .Include(u => u.AnnotatedPublications).ThenInclude(au => au.Publication).ThenInclude(p => p.User)
                                    .Include(u => u.AnnotatedPublications).ThenInclude(au => au.Publication).ThenInclude(p => p.Zone)
                                    .Include(u => u.AnnotatedPublications).ThenInclude(au => au.Publication).ThenInclude(p => p.FinalizeReason)
                                    .Where(u => u.Id_Google == googleId).First<User>()
                                    .AnnotatedPublications
                                    .Select(ap => ap.Publication).ToList();
        }

        public List<User> GetAnnotatedUsersByPublication(int publicationId)
        {
            return _dbContext.Publications
                                       .Include(u => u.AnnotatedUsers).ThenInclude(au => au.User)
                                       .Where(p => p.PublicationId == publicationId).First<Publication>()
                                       .AnnotatedUsers
                                       .Select(au => au.User).ToList();
        }

        public int FinalizePublication(int id, FinalizePublication finalizePublication){
            var pub = _dbContext.Publications.Where(p=>p.PublicationId==id).SingleOrDefault();
                pub.FinalizeReason=finalizePublication;
                return _dbContext.SaveChanges();
        }

        public string GetFcmTokenByUser(string googleId){
            var user =_dbContext.Users.Where(u=>u.Id_Google==googleId).SingleOrDefault();
            if(user!=null){
                return user.Fcm_token;
            }else{
                return "";
            }
        }
    }
}