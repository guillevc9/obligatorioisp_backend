using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ObligatorioBackEnd.Models
{
    public class User
    {
        [Key]
        public int UserId { get; set; }
        [Required]
        public string Name {get;set;}
        public string LastName {get;set;}
        [Required]
        public string Mail {get;set;}
        [Required]
        public string Id_Google {get;set;}
        public string Fcm_token {get;set;}
        public ICollection<AnnotatedUsers> AnnotatedPublications {get;set;}

        public User(){}


        public override string ToString(){
            return String.Format("Id: {0}, Nombre: {1}, Apellido: {2}, Mail: {3}, Google_ID: {4}",UserId,Name,LastName,Mail,Id_Google);
        }

        
    }
}