﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ObligatorioBackEnd.Migrations
{
    public partial class add_fcm_token : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Fcm_token",
                table: "Usuarios",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Fcm_token",
                table: "Usuarios");
        }
    }
}
