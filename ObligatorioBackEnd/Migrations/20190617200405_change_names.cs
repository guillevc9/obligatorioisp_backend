﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ObligatorioBackEnd.Migrations
{
    public partial class change_names : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AnnotatedUsers_Publicaciones_PublicacionId",
                table: "AnnotatedUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_AnnotatedUsers_Usuarios_UsuarioId",
                table: "AnnotatedUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_FinalizePublication_Publicaciones_IdPublicacion",
                table: "FinalizePublication");

            migrationBuilder.DropForeignKey(
                name: "FK_FinalizePublication_Usuarios_UserAssignedUsuarioId",
                table: "FinalizePublication");

            migrationBuilder.DropForeignKey(
                name: "FK_Question_Publicaciones_IdPublicacion",
                table: "Question");

            migrationBuilder.DropTable(
                name: "Publicaciones");

            migrationBuilder.DropTable(
                name: "Categorias");

            migrationBuilder.DropTable(
                name: "Usuarios");

            migrationBuilder.RenameColumn(
                name: "IdPublicacion",
                table: "Question",
                newName: "IdPublication");

            migrationBuilder.RenameIndex(
                name: "IX_Question_IdPublicacion",
                table: "Question",
                newName: "IX_Question_IdPublication");

            migrationBuilder.RenameColumn(
                name: "UserAssignedUsuarioId",
                table: "FinalizePublication",
                newName: "UserAssignedUserId");

            migrationBuilder.RenameColumn(
                name: "IdPublicacion",
                table: "FinalizePublication",
                newName: "IdPublication");

            migrationBuilder.RenameIndex(
                name: "IX_FinalizePublication_UserAssignedUsuarioId",
                table: "FinalizePublication",
                newName: "IX_FinalizePublication_UserAssignedUserId");

            migrationBuilder.RenameIndex(
                name: "IX_FinalizePublication_IdPublicacion",
                table: "FinalizePublication",
                newName: "IX_FinalizePublication_IdPublication");

            migrationBuilder.RenameColumn(
                name: "UsuarioId",
                table: "AnnotatedUsers",
                newName: "UserId");

            migrationBuilder.RenameColumn(
                name: "PublicacionId",
                table: "AnnotatedUsers",
                newName: "PublicationId");

            migrationBuilder.RenameIndex(
                name: "IX_AnnotatedUsers_UsuarioId",
                table: "AnnotatedUsers",
                newName: "IX_AnnotatedUsers_UserId");

            migrationBuilder.AlterColumn<string>(
                name: "Text",
                table: "Question",
                maxLength: 140,
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Answer",
                table: "Question",
                maxLength: 140,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    LastName = table.Column<string>(nullable: true),
                    Mail = table.Column<string>(nullable: false),
                    Id_Google = table.Column<string>(nullable: false),
                    Fcm_token = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "Publications",
                columns: table => new
                {
                    PublicationId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(maxLength: 40, nullable: false),
                    Description = table.Column<string>(maxLength: 200, nullable: true),
                    Address = table.Column<string>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    ZoneId = table.Column<int>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    Photos = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Publications", x => x.PublicationId);
                    table.ForeignKey(
                        name: "FK_Publications_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Publications_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Publications_Zones_ZoneId",
                        column: x => x.ZoneId,
                        principalTable: "Zones",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Publications_CategoryId",
                table: "Publications",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Publications_UserId",
                table: "Publications",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Publications_ZoneId",
                table: "Publications",
                column: "ZoneId");

            migrationBuilder.AddForeignKey(
                name: "FK_AnnotatedUsers_Publications_PublicationId",
                table: "AnnotatedUsers",
                column: "PublicationId",
                principalTable: "Publications",
                principalColumn: "PublicationId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AnnotatedUsers_Users_UserId",
                table: "AnnotatedUsers",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_FinalizePublication_Publications_IdPublication",
                table: "FinalizePublication",
                column: "IdPublication",
                principalTable: "Publications",
                principalColumn: "PublicationId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FinalizePublication_Users_UserAssignedUserId",
                table: "FinalizePublication",
                column: "UserAssignedUserId",
                principalTable: "Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Question_Publications_IdPublication",
                table: "Question",
                column: "IdPublication",
                principalTable: "Publications",
                principalColumn: "PublicationId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AnnotatedUsers_Publications_PublicationId",
                table: "AnnotatedUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_AnnotatedUsers_Users_UserId",
                table: "AnnotatedUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_FinalizePublication_Publications_IdPublication",
                table: "FinalizePublication");

            migrationBuilder.DropForeignKey(
                name: "FK_FinalizePublication_Users_UserAssignedUserId",
                table: "FinalizePublication");

            migrationBuilder.DropForeignKey(
                name: "FK_Question_Publications_IdPublication",
                table: "Question");

            migrationBuilder.DropTable(
                name: "Publications");

            migrationBuilder.DropTable(
                name: "Categories");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.RenameColumn(
                name: "IdPublication",
                table: "Question",
                newName: "IdPublicacion");

            migrationBuilder.RenameIndex(
                name: "IX_Question_IdPublication",
                table: "Question",
                newName: "IX_Question_IdPublicacion");

            migrationBuilder.RenameColumn(
                name: "UserAssignedUserId",
                table: "FinalizePublication",
                newName: "UserAssignedUsuarioId");

            migrationBuilder.RenameColumn(
                name: "IdPublication",
                table: "FinalizePublication",
                newName: "IdPublicacion");

            migrationBuilder.RenameIndex(
                name: "IX_FinalizePublication_UserAssignedUserId",
                table: "FinalizePublication",
                newName: "IX_FinalizePublication_UserAssignedUsuarioId");

            migrationBuilder.RenameIndex(
                name: "IX_FinalizePublication_IdPublication",
                table: "FinalizePublication",
                newName: "IX_FinalizePublication_IdPublicacion");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "AnnotatedUsers",
                newName: "UsuarioId");

            migrationBuilder.RenameColumn(
                name: "PublicationId",
                table: "AnnotatedUsers",
                newName: "PublicacionId");

            migrationBuilder.RenameIndex(
                name: "IX_AnnotatedUsers_UserId",
                table: "AnnotatedUsers",
                newName: "IX_AnnotatedUsers_UsuarioId");

            migrationBuilder.AlterColumn<string>(
                name: "Text",
                table: "Question",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 140);

            migrationBuilder.AlterColumn<string>(
                name: "Answer",
                table: "Question",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 140,
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "Categorias",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nombre = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categorias", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Usuarios",
                columns: table => new
                {
                    UsuarioId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Apellido = table.Column<string>(nullable: true),
                    Fcm_token = table.Column<string>(nullable: true),
                    Id_Google = table.Column<string>(nullable: false),
                    Mail = table.Column<string>(nullable: false),
                    Nombre = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Usuarios", x => x.UsuarioId);
                });

            migrationBuilder.CreateTable(
                name: "Publicaciones",
                columns: table => new
                {
                    PublicacionId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    CategoriaId = table.Column<int>(nullable: false),
                    Descripcion = table.Column<string>(maxLength: 200, nullable: true),
                    Direccion = table.Column<string>(nullable: false),
                    Photos = table.Column<int>(nullable: false),
                    Titulo = table.Column<string>(maxLength: 40, nullable: false),
                    UsuarioId = table.Column<int>(nullable: false),
                    ZoneId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Publicaciones", x => x.PublicacionId);
                    table.ForeignKey(
                        name: "FK_Publicaciones_Categorias_CategoriaId",
                        column: x => x.CategoriaId,
                        principalTable: "Categorias",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Publicaciones_Usuarios_UsuarioId",
                        column: x => x.UsuarioId,
                        principalTable: "Usuarios",
                        principalColumn: "UsuarioId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Publicaciones_Zones_ZoneId",
                        column: x => x.ZoneId,
                        principalTable: "Zones",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Publicaciones_CategoriaId",
                table: "Publicaciones",
                column: "CategoriaId");

            migrationBuilder.CreateIndex(
                name: "IX_Publicaciones_UsuarioId",
                table: "Publicaciones",
                column: "UsuarioId");

            migrationBuilder.CreateIndex(
                name: "IX_Publicaciones_ZoneId",
                table: "Publicaciones",
                column: "ZoneId");

            migrationBuilder.AddForeignKey(
                name: "FK_AnnotatedUsers_Publicaciones_PublicacionId",
                table: "AnnotatedUsers",
                column: "PublicacionId",
                principalTable: "Publicaciones",
                principalColumn: "PublicacionId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AnnotatedUsers_Usuarios_UsuarioId",
                table: "AnnotatedUsers",
                column: "UsuarioId",
                principalTable: "Usuarios",
                principalColumn: "UsuarioId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FinalizePublication_Publicaciones_IdPublicacion",
                table: "FinalizePublication",
                column: "IdPublicacion",
                principalTable: "Publicaciones",
                principalColumn: "PublicacionId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FinalizePublication_Usuarios_UserAssignedUsuarioId",
                table: "FinalizePublication",
                column: "UserAssignedUsuarioId",
                principalTable: "Usuarios",
                principalColumn: "UsuarioId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Question_Publicaciones_IdPublicacion",
                table: "Question",
                column: "IdPublicacion",
                principalTable: "Publicaciones",
                principalColumn: "PublicacionId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
