using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace ObligatorioBackEnd.Dto
{
    public class PublicationPhotosDto
    {
        public int idPublication {get;set;}
        public List<IFormFile> Photos{get;set;}

    }
}