using System.ComponentModel.DataAnnotations;

namespace ObligatorioBackEnd.Models
{
    public class Zone
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}