using ObligatorioBackEnd.Models;

namespace ObligatorioBackEnd.Dto
{
    public class CategoryDto
    {
        public string Title{get;set;}
        public CategoryDto(){}

        public CategoryDto(Category cat){
            Title=cat.Name;
        }

    }
}