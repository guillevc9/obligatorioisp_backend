﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ObligatorioBackEnd.Migrations
{
    public partial class mod_usuario : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Id_Goolge",
                table: "Usuarios",
                newName: "Id_Google");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Id_Google",
                table: "Usuarios",
                newName: "Id_Goolge");
        }
    }
}
