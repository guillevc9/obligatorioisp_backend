using System.Linq;
using System;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Google.Apis.Auth;
using ObligatorioBackEnd.Models;
using System.Threading.Tasks;
using ObligatorioBackEnd.DataContext;
using System.Collections.Generic;
using ObligatorioBackEnd.Handlers;

namespace ObligatorioBackEnd
{
    public class TokenHandler :ITokenHandler
    {
        public User GetUser(String token)
        {
            User userObtain = new User();

            var jwtHandler = new JwtSecurityTokenHandler();
            var jwtInput = token;

            var readableToken = jwtHandler.CanReadToken(jwtInput);
            if (readableToken != true)
            {
                userObtain = null;
            }
            else
            {
                var jwt_token = jwtHandler.ReadJwtToken(jwtInput);                
                //Extract the payload of the JWT
                var claims = jwt_token.Claims;
                
                foreach (Claim c in claims)
                {
                    //jwtPayload = '"' + c.Type + "\":\"" + c.Value + "\",";
                    //Debug.WriteLine("-------------PAYLOAD: " + jwtPayload);    
                    if(c.Type=="email"){
                        userObtain.Mail=c.Value;
                    }
                    if(c.Type=="given_name"){
                        userObtain.Name=c.Value;
                    }
                    if(c.Type=="family_name"){
                        userObtain.LastName=c.Value;
                    }
                     if(c.Type=="sub"){
                        userObtain.Id_Google=c.Value;
                    }
                }
            }
            return userObtain;
        }

        public string GetGoogleId(String token)
        {
            var jwtHandler = new JwtSecurityTokenHandler();
            var jwt_token = jwtHandler.ReadJwtToken(token);                
            var claims = jwt_token.Claims;
            //Retrieve the IdGoogle of the user
            return claims.Where(c=>c.Type=="sub").SingleOrDefault().Value.ToString();;
        }

        public async Task<bool> IsValid(String token){
            Boolean validGoogleToken = false;
            String authToken = token;

            //Verifico si esta en token obtenidos
            TokenMap tokenDictionary = TokenMap.Instance;
            if (tokenDictionary.Exists(authToken))
            {
                //Si existe continuo con la peticion original
                return true;
            }
            else
            {
                try
                {
                    await GoogleJsonWebSignature.ValidateAsync(authToken);
                    validGoogleToken = true;
                }
                // No se valido contra Google
                catch (InvalidJwtException e)
                {
                    String error = String.Format("{0} - {1}", e.Message, e.StackTrace);
                    //Debug.WriteLine(error);
                    Debug.WriteLine($"Obligatorio_FF {DateTime.Now}: Token: " + error);
                    return false;
                }

                //Si no existe cheque el token contra Google
                if (validGoogleToken)
                {
                    //Si Google valida el token, entonces ahora lo valido contra mi api key
                    var jwtHandler = new JwtSecurityTokenHandler();
                    var readableToken = jwtHandler.CanReadToken(authToken);
                    if (readableToken != true)
                    {
                        return false;
                    }
                    else
                    {
                        var jwt_token = jwtHandler.ReadJwtToken(authToken);
                        //Extract the headers of the JWT
                        List<Claim> claims = jwt_token.Claims.ToList();
                        //Claim aud = claims
                        Claim claimAud = claims.Where(c => c.Type == "aud").First<Claim>();
                        //Chequeo aud contra mi api key
                        if (claimAud.Value == "560935163238-ouceh5f4bmbdfk50ir0dpf96oiubqgu2.apps.googleusercontent.com")
                        {
                            Claim claimGoogleId = claims.Where(c => c.Type == "sub").First<Claim>();
                            EFDataContext _dbContext = new EFDataContext();

                            if (!_dbContext.Users.Any(u => u.Id_Google == claimGoogleId.Value))
                            {
                                return false;
                            }
                            //El metodo Token_agregar se fija si existe el id reemplaza el token, si no 
                            //lo agrega como nuevo
                            TokenMap.Instance.Add(claimGoogleId.Value, authToken);
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    return false;
                }
            }
        }
    }
}